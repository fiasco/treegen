#define URHO3D_PIXEL_NEED_TEXCOORD
#define URHO3D_PIXEL_NEED_TEXCOORD1


#include "_Material.glsl"


VERTEX_OUTPUT_HIGHP(vec2 vTexCoord2);


#ifdef URHO3D_VERTEX_SHADER
VERTEX_INPUT(vec2 iTexCoord2);
void main()
{
    VertexTransform vertexTransform = GetVertexTransform();
    FillVertexOutputs(vertexTransform);
    vTexCoord2 = iTexCoord2;
}
#endif


void _BarkGetFragmentAlbedoSpecular(const half oneMinusReflectivity,
                                    out half4 albedo,
                                    out half3 specular)
{
#ifdef URHO3D_MATERIAL_HAS_DIFFUSE
    float factor = float(vColor[0]) / 255.0f;
    half4 albedoInput = mix(texture2D(sDiffMap, vTexCoord),
                            texture2D(sDiffMap, vTexCoord2), factor);
    #ifdef ALPHAMASK
        if (albedoInput.a < 0.5)
            discard;
    #endif

    albedo = GammaToLightSpaceAlpha(cMatDiffColor) * DiffMap_ToLight(albedoInput);
#else
    albedo = GammaToLightSpaceAlpha(cMatDiffColor);
#endif

#ifdef URHO3D_PHYSICAL_MATERIAL
    specular = albedo.rgb * (1.0 - oneMinusReflectivity);
    albedo.rgb *= oneMinusReflectivity;
#else
    #ifdef URHO3D_MATERIAL_HAS_SPECULAR
        specular = GammaToLightSpace(cMatSpecColor.rgb * texture2D(sSpecMap, vTexCoord).rgb);
    #else
        specular = GammaToLightSpace(cMatSpecColor.rgb);
    #endif
#endif

#ifdef URHO3D_PREMULTIPLY_ALPHA
    albedo.rgb *= albedo.a;
    #ifdef URHO3D_PHYSICAL_MATERIAL
        albedo.a = 1.0 - oneMinusReflectivity + albedo.a * oneMinusReflectivity;
    #endif
#endif
}


#define BarkFillSurfaceAlbedoSpecular(surfaceData) \
    _BarkGetFragmentAlbedoSpecular(surfaceData.oneMinusReflectivity, \
                                   surfaceData.albedo, \
                                   surfaceData.specular)


#ifdef URHO3D_PIXEL_SHADER


void main()
{
#ifdef URHO3D_DEPTH_ONLY_PASS
    DefaultPixelShader();
#else
    SurfaceData surfaceData;

    FillSurfaceCommon(surfaceData);
    FillSurfaceNormal(surfaceData);
    FillSurfaceMetallicRoughnessOcclusion(surfaceData);
    FillSurfaceReflectionColor(surfaceData);
    FillSurfaceBackground(surfaceData);
    BarkFillSurfaceAlbedoSpecular(surfaceData);
    FillSurfaceEmission(surfaceData);

    half3 surfaceColor = GetSurfaceColor(surfaceData);
    half surfaceAlpha = GetSurfaceAlpha(surfaceData);
    gl_FragColor = GetFragmentColorAlpha(surfaceColor, surfaceAlpha, surfaceData.fogFactor);
#endif
}
#endif
