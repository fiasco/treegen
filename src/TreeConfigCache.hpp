#include "TreeGenSettings.hpp"
#include <UrhoBits/TreeGenerator/TreeConfig.hpp>

using namespace V2; // TODO rename it?

struct TreeConfigCache {
    TreeConfigCache (Settings2&);

    UrhoBits::TreeConfig* getTreeConfig () {
        return &_ch;
    }

    std::string leafTextureName;
    std::string leafObjectName;
    std::string leafObjectTextureName;

    std::string leaves_shape;
    std::string smoothing_algorithm;

private:
    UrhoBits::TreeConfig _ch;

    Settings2& _cfg;
};

